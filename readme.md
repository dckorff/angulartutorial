#AngularJS tutorial
git clone file:////csildkorff02/share/angularTutorial
or
git clone https://bitbucket.org/dckorff/angulartutorial.git
###OMG Why.
- We now have applications that are written in Angular and you might have to fix one some day.
- Popularity = More documentation, more tools, more features (and more jobs)
https://www.google.com/trends/explore#q=angularjs%2C%20extjs%2C%20knockoutjs%2C%20emberjs%2C%20reactjs&cmpt=q&tz=Etc%2FGMT%2B5
- Easy to get started with
- Full single-page application framework (or just use it for individual components)
- 2-way binding magic
- Plays well with other tools and frameworks. Can wrap JQuery plugins, ExtJs components, and anything else.

##Simple ToDo app

###Step 1 - Boilerplate
- angular app and controller definitions
- ng-app and ng-controller

###Step 2 - Two-way databinding
- scope.newItem
- ng-model
- {{newItem}}
- initializing to asdf changes the textbox to asdf, and changing the textbox changes everything that's bound to it

###Step 3 - Adding a function
- ng-click
- controller function
- add a function to a controller and call it from the view
- the newItem value is still bound (has the value of the textbox) when adding it from the controller

### Step 4 - ng-repeat
- ng-repeat
- Functional - no need to manually update the dom when new objects are added to the array

###Step 5 - Remove button
- remove - ng-repeat is still bound
- no need to manually remove from elements from the dom, this happens automatically

###Step 6 - MVC and Routes
- pull from source (don't type)
- Basic syntax for setting up routes
- 3 different ways to create a template
	- a file
	- inline html
	- a template inside a script tag
- ng-view

###Step 7 - CATS! (and the digest cycle)
- pull from source (don't type)
- loadCats function
- Get Cats button
- Cat pics!
- Digest loop and $scope.$apply()

###Step 8 - Directives
- Custom dom elements
